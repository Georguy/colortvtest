# ColorTVTest
<h1 align="center">
  <p align="center">
    <img src="https://img.shields.io/badge/Language-Swift-blue.svg">
    <a href="LICENSE.md"><img src="https://img.shields.io/badge/License-MIT-brightgreen.svg"></a>
    <img src="https://img.shields.io/badge/Type-Entrance test-orange.svg">
  </p>
</h1>
<p align="center"><img src="Resources/Map.PNG" height="400"> <img src="Resources/Info.PNG" height="400"></p>

## Description
iOS client for Weatherbit(https://www.weatherbit.io/). 

Two screens: map and details viewer.
The app doesn't have any granular architecture (e.g Viper, or YARCH), cause the logic isn't massive. Also some entities should have interfaces/protocols, but in the current state, it's extra (no enough time for tests and modules will not be reused).

## Restrictions
Application is in demo mode and is rate-limited to 500 requests per day. After this limit, API will send response with error.

## Deltas
* Show loading state;
* Show weather for the current location on Map screen;
* Support dark mode;
* Write unit-tests.

## Requirements
- iOS 12 or later
- Swift version 5.0
- A Mac with Xcode 11 or later
