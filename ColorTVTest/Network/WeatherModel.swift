//
//  WeatherModel.swift
//  ColorTVTest
//
//  Created by Georgij Emelyanov on 05.07.2020.
//  Copyright © 2020 gemelyanov.com. All rights reserved.
//

import Foundation

struct WeatherModel: Codable {
  private struct WeatherInfo: Codable {
    let icon: String
    let code: String
    let description: String
  }
  
  let lat: Double
  let lon: Double
  let city: String
  let pressure: Double
  let windSpeed: Double
  let temperature: Double
  let humidity: Double
  let description: String
  let icon: String
  
  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    lat = try container.decode(Double.self, forKey: .lat)
    lon = try container.decode(Double.self, forKey: .lon)
    city = try container.decode(String.self, forKey: .city)
    pressure = try container.decode(Double.self, forKey: .pressure)
    windSpeed = try container.decode(Double.self, forKey: .windSpeed)
    temperature = try container.decode(Double.self, forKey: .temperature)
    humidity = try container.decode(Double.self, forKey: .humidity)
    
    let info = try container.decode(WeatherInfo.self, forKey: .info)
    description = info.description
    icon = info.icon
  }
  
  func encode(to encoder: Encoder) throws { }
}

private extension WeatherModel {
  enum CodingKeys: String, CodingKey {
    case lat
    case lon
    case city = "city_name"
    case pressure = "pres"
    case windSpeed = "wind_spd"
    case temperature = "temp"
    case humidity = "rh"
    case info = "weather"
  }
}

