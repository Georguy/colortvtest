//
//  WeatherProvider.swift
//  ColorTVTest
//
//  Created by Georgij Emelyanov on 05.07.2020.
//  Copyright © 2020 gemelyanov.com. All rights reserved.
//

import Foundation

final class WeatherProvider {
  
  var onFetchWeather: ((WeatherModel) -> Void)?
  var onFetchWeatherFailure: ((_ error: Error) -> Void)?
  
  private lazy var weatherOperationQueue: OperationQueue = {
    let operation = OperationQueue(with: "com.ColorTVTest.operationQueue")
    operation.maxConcurrentOperationCount = 1
    return operation
  }()
  
  func fetchWeatherFot(lat: Double, lon: Double) {
    let operation = CurrentWeatherOperation(lat: lat, lon: lon)
    operation.getWeatherCompletion = { [weak self] result in
      guard let self = self else { return }
      switch result {
      case let .success(weather):
        DispatchQueue.main.async { [weak self] in
          self?.onFetchWeather?(weather)
        }
      case let .failure(error):
        DispatchQueue.main.async { [weak self] in
          self?.onFetchWeatherFailure?(error)
        }
      }
    }
    weatherOperationQueue.addOperation(operation)
  }
  
}
