//
//  CurrentWeatherOperation.swift
//  ColorTVTest
//
//  Created by Georgij Emelyanov on 05.07.2020.
//  Copyright © 2020 gemelyanov.com. All rights reserved.
//

import Foundation

class CurrentWeatherOperation: OpenWeatherOperation {
  
  var getWeatherCompletion: (Result<WeatherModel, Error>) -> Void = { _ in }
  
  private var weather: WeatherModel?
  private var lat: Double
  private var lon: Double
  
  required init(lat: Double, lon: Double) {
    self.lat = lat
    self.lon = lon
    super.init()
  }
  
  override var endpoint: String {
    return "/v2.0/current"
  }
  
  override var queryItems: [URLQueryItem] {
    return [
      URLQueryItem(name: "lat", value: "\(lat)"),
      URLQueryItem(name: "lon", value: "\(lon)"),
      URLQueryItem(name: "key", value: ApiConfig.apiKey),
    ]
  }
  
  override func processResponseData(_ data: Data?) {
    if let weather = weatherFromResponseData(data) {
      self.weather = weather
    }
    completed()
  }
  
  private func weatherFromResponseData(_ data: Data?) -> WeatherModel? {
    guard let data = data else { return nil }

    do {
      return try JSONDecoder().decode(ResponseData.self, from: data).data[0]
    } catch {
      finish(with: error)
      return nil
    }
  }
  
  override func completed() {
    if let error = self.error {
      self.getWeatherCompletion(.failure(error))
    } else if let weather = self.weather {
      self.getWeatherCompletion(.success(weather))
    }
    super.completed()
  }
}

private extension CurrentWeatherOperation {
  struct ResponseData: Codable {
    enum CodingKeys: String, CodingKey {
      case data
    }
    
    let data: [WeatherModel]
    
    init(from decoder: Decoder) throws {
      let container = try decoder.container(keyedBy: CodingKeys.self)
      data = try container.decode([WeatherModel].self, forKey: .data)
    }
    
    func encode(to encoder: Encoder) throws { }
  }
}
