//
//  OpenWeatherOperation.swift
//  ColorTVTest
//
//  Created by Georgij Emelyanov on 05.07.2020.
//  Copyright © 2020 gemelyanov.com. All rights reserved.
//

import Foundation

enum ApiConfig {
  static var baseApiPath: String {
    return "https://api.weatherbit.io"
  }
  
  static var apiKey: String {
    return "7ea27ae4fb5541feba9be662f4506735"
  }
}

class OpenWeatherOperation: NetworkOperation {
  
  private(set) var jsonResponse: Any?
  
  override func prepareURLComponents() -> URLComponents? {
    guard let baseApiPath = URL(string: ApiConfig.baseApiPath) else { return nil }
    
    var urlComponents = URLComponents(url: baseApiPath, resolvingAgainstBaseURL: true)
    urlComponents?.path = endpoint
    urlComponents?.queryItems = queryItems
    return urlComponents
  }
  
  override func processResponseData(_ data: Data?) {
    if let error = error {
      finish(with: error)
      return
    }
    guard let data = data else { return }
    
    do {
      jsonResponse = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.init(rawValue: 0))
      processJSONResponse()
    } catch {
      finish(with: RequestError.invalidJSONResponse)
    }
  }
  
  func processJSONResponse() {
    if let error = error {
      finish(with: error)
    } else {
      completed()
    }
  }
}

extension OpenWeatherOperation {
  enum RequestError: Error {
    case invalidJSONResponse
    
    var localizedDescription: String {
      switch self {
      case .invalidJSONResponse:
        return "Invalid JSON response."
      }
    }
  }
}
