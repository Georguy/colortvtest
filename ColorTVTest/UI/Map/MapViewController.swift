//
//  MapViewController.swift
//  ColorTVTest
//
//  Created by Georgij Emelyanov on 05.07.2020.
//  Copyright © 2020 gemelyanov.com. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MapViewController: UIViewController {
  
  private var locationManager = CLLocationManager()
  private var currentLocation = "Your Location"
  private var isShownOnce = false
  
  private lazy var mapView: MKMapView = {
    let mapView = MKMapView(frame: .zero)
    mapView.translatesAutoresizingMaskIntoConstraints = false
    mapView.isRotateEnabled = false
    return mapView
  }()
  
  private lazy var infoView: ErrorBanner = ErrorBanner.fromNib()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    configureLayout()
    
    locationManager.delegate = self
    locationManager.desiredAccuracy = kCLLocationAccuracyKilometer
    locationManager.requestWhenInUseAuthorization()
    
    if CLLocationManager.locationServicesEnabled() {
      locationManager.startUpdatingLocation()
    }
    
    let doubleTapGR = UITapGestureRecognizer(target: self, action: #selector(mapWasTappedTwice(_:)))
    doubleTapGR.numberOfTapsRequired = 2
    doubleTapGR.delegate = self
    mapView.addGestureRecognizer(doubleTapGR)
  }
  
  @objc
  private func mapWasTappedTwice(_ gestureRecognizer: UIGestureRecognizer) {
    let touchPoint = gestureRecognizer.location(in: mapView)
    let coordinate = mapView.convert(touchPoint, toCoordinateFrom: mapView)
    
    let vc = WeatherViewController(lat: coordinate.latitude, lon: coordinate.longitude)
    let navVC = UINavigationController(rootViewController: vc)
    navVC.view.backgroundColor = .white
    present(navVC, animated: true, completion: nil)
  }
}

extension MapViewController: UIGestureRecognizerDelegate {
  func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
    return gestureRecognizer is UITapGestureRecognizer
  }
}

extension MapViewController: CLLocationManagerDelegate {
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    guard let currentLocation = locations.first else { return }
    
    
    let annotation = MKPointAnnotation()
    let center = CLLocationCoordinate2D(latitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude)
    annotation.coordinate = center
    annotation.title = usersLocation(lat: center.latitude, lon: center.longitude)
    mapView.addAnnotation(annotation)
    
    if !isShownOnce {
      let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
      mapView.setRegion(region, animated: true)
      isShownOnce = true
    }
  }
}

private extension MapViewController {
  func configureLayout() {
    view.addSubview(mapView)
    
    NSLayoutConstraint.activate([
      mapView.leftAnchor.constraint(equalTo: view.leftAnchor),
      mapView.topAnchor.constraint(equalTo: view.topAnchor),
      mapView.rightAnchor.constraint(equalTo: view.rightAnchor),
      mapView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
    ])
  }
  
  func usersLocation(lat: CLLocationDegrees, lon: CLLocationDegrees) -> String {
    let location = CLLocation(latitude: lat, longitude: lon)
    let geoCoder = CLGeocoder()
    
    geoCoder.reverseGeocodeLocation(location) { [weak self] (placemarks, _)  in
      if let placemark = placemarks?.first {
        let name = placemark.name ?? "Your"
        let city = placemark.subAdministrativeArea ?? "Location"
        self?.currentLocation = "\(name), \(city)"
      }
    }
    return currentLocation
  }
}
