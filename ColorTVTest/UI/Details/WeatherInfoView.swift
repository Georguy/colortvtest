//
//  WeatherInfoView.swift
//  ColorTVTest
//
//  Created by Georgij Emelyanov on 05.07.2020.
//  Copyright © 2020 gemelyanov.com. All rights reserved.
//

import UIKit

struct WeatherInfoViewModel {
  let image: UIImage?
  let description: String
  let humidity: String
  let temperature: String
  let pressure: String
  let wind: String
  let city: String
  
  var shortInfo: String {
    return "\(temperature), \(pressure), \(wind)"
  }
  
  init(weatherModel: WeatherModel) {
    image = UIImage(named: weatherModel.icon)
    description = weatherModel.description
    
    humidity = "\(weatherModel.humidity)%"
    temperature = "\(weatherModel.temperature)˚C"
    pressure = "\(weatherModel.pressure)mb"
    wind = "\(weatherModel.windSpeed)m/s"
    
    city = weatherModel.city
  }
}

final class WeatherInfoView: UIView {
  
  @IBOutlet private var imageView: UIImageView!
  @IBOutlet private var descriptionLabel: UILabel!
  @IBOutlet private var humidityLabel: UILabel!
  @IBOutlet private var temperatureLabel: UILabel!
  @IBOutlet private var pressureLabel: UILabel!
  @IBOutlet private var windLabel: UILabel!
  
  var viewModel: WeatherInfoViewModel? {
    didSet {
      guard let viewModel = viewModel else { return }
      imageView.image = viewModel.image
      descriptionLabel.text = viewModel.description
      humidityLabel.text = viewModel.humidity
      temperatureLabel.text = viewModel.temperature
      pressureLabel.text = viewModel.pressure
      windLabel.text = viewModel.wind
    }
  }
}
