//
//  WeatherViewController.swift
//  ColorTVTest
//
//  Created by Georgij Emelyanov on 05.07.2020.
//  Copyright © 2020 gemelyanov.com. All rights reserved.
//

import UIKit

class WeatherViewController: UIViewController {
  
  private let provider = WeatherProvider()
  
  private var weatherView: WeatherInfoView = {
    let view = WeatherInfoView.fromNib()
    view.translatesAutoresizingMaskIntoConstraints = false
    return view
  }()
  
  private lazy var errorView: ErrorBanner = {
    let view = ErrorBanner.fromNib()
    view.translatesAutoresizingMaskIntoConstraints = false
    view.onRepeat = { [weak self] in
      guard let self = self else { return }
      self.provider.fetchWeatherFot(lat: self.lat, lon: self.lon)
    }
    return view
  }()
  
  private let lat: Double
  private let lon: Double
  
  private var errorBannerTopConstraint: NSLayoutConstraint!
  
  required init(lat: Double, lon: Double) {
    self.lat = lat
    self.lon = lon
    super.init(nibName: nil, bundle: nil)
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func loadView() {
    super.loadView()
    configureLayout()
    addLeftBarButtonItem()
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    provider.onFetchWeather = { [weak self] weather in
      self?.title = weather.city
      self?.weatherView.viewModel = WeatherInfoViewModel(weatherModel: weather)
      self?.weatherView.isHidden = false
      self?.errorView.isHidden = true
    }
    
    provider.onFetchWeatherFailure = { [weak self] error in
      self?.errorView.title = error.localizedDescription
      self?.errorView.isHidden = false
      self?.errorBannerTopConstraint.constant = 20
      UIView.animate(withDuration: 0.3) {
        self?.view.layoutIfNeeded()
      }
    }
    
    provider.fetchWeatherFot(lat: lat, lon: lon)
  }
}

private extension WeatherViewController {
  func configureLayout() {
    view.addSubview(weatherView)
    view.addSubview(errorView)
    weatherView.isHidden = true
    errorView.isHidden = true
    
    errorBannerTopConstraint = errorView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: -100)
    
    NSLayoutConstraint.activate([
      weatherView.leftAnchor.constraint(equalTo: view.leftAnchor),
      weatherView.topAnchor.constraint(equalTo: view.topAnchor),
      weatherView.rightAnchor.constraint(equalTo: view.rightAnchor),
      weatherView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
      
      errorView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20),
      errorView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20),
      errorBannerTopConstraint,
    ])
  }
  
  func addLeftBarButtonItem() {
    let item: UIBarButtonItem
    if #available(iOS 13.0, *) {
      item = UIBarButtonItem(barButtonSystemItem: .close, target: self, action: #selector(close))
    } else {
      item = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(close))
    }
    navigationItem.leftBarButtonItem = item
  }
  
  @objc
  func close() {
    self.navigationController?.dismiss(animated: true, completion: nil)
  }
}
