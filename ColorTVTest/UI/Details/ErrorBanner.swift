//
//  ErrorBanner.swift
//  ColorTVTest
//
//  Created by Georgij Emelyanov on 05.07.2020.
//  Copyright © 2020 gemelyanov.com. All rights reserved.
//

import UIKit

class ErrorBanner: UIView {
  var title: String? {
    didSet { titleLabel.text = title }
  }
  
  var onRepeat: (() -> Void)?
  
  @IBOutlet private var titleLabel: UILabel!
  
  @IBAction private func repeatButtonPressed() {
    onRepeat?()
  }
}
