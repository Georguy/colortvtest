//
//  UIView+Extensions.swift
//  ColorTVTest
//
//  Created by Georgij Emelyanov on 05.07.2020.
//  Copyright © 2020 gemelyanov.com. All rights reserved.
//

import UIKit

extension UIView {
  class func fromNib(nibNameOrNil: String? = nil) -> Self {
      return fromNib(nibNameOrNil: nibNameOrNil, type: self)!
  }
  
  class func fromNib<T: UIView>(nibNameOrNil: String? = nil, type: T.Type) -> T? {
      var view: T?
      let name: String
      if let nibName = nibNameOrNil {
          name = nibName
      } else {
          name = nibName
      }
      let nibViews = Bundle.main.loadNibNamed(name, owner: nil, options: nil)
      for nibView in nibViews! {
          if let tog = nibView as? T {
              view = tog
          }
      }
      return view
  }
  
  class var nibName: String {
      let name = "\(self)".components(separatedBy: ".").first ?? ""
      return name
  }
  
  class var nib: UINib? {
      if Bundle.main.path(forResource: nibName, ofType: "nib") != nil {
          return UINib(nibName: nibName, bundle: nil)
      } else {
          return nil
      }
  }
}
