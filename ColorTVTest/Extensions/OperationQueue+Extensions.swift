//
//  OperationQueue+Extensions.swift
//  ColorTVTest
//
//  Created by Georgij Emelyanov on 05.07.2020.
//  Copyright © 2020 gemelyanov.com. All rights reserved.
//

import Foundation

extension OperationQueue {
  convenience init(with name: String) {
    self.init()
    self.name = name
  }
}
